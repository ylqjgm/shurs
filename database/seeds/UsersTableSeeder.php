<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = factory(User::class)->times(700)->make();
        User::insertOrIgnore($users->toArray());

        $admin           = User::find(1);
        $admin->username = 'admin';
        $admin->nicname  = '管理员';
        $admin->email    = 'admin@admin.com';
        $admin->save();
    }
}
