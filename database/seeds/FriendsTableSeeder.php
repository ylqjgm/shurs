<?php

use App\Models\Friend;
use Illuminate\Database\Seeder;

class FriendsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $friends = factory(Friend::class)->times(60)->make();
        Friend::insertOrIgnore($friends->toArray());
    }
}
