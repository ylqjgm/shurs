<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->call(
            [
                CategoriesTableSeeder::class,
                UsersTableSeeder::class,
                FriendsTableSeeder::class,
                AuthorsTableSeeder::class,
                BooksTableSeeder::class,
                ChaptersTableSeeder::class,
                AttachesTableSeeder::class,
                FollowsTableSeeder::class,
            ]
        );
        Model::unguard();
    }
}
