<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert(
            [
                [
                    'parent_id' => 0,
                    'title'     => '奇幻玄幻',
                    'slug'      => 'qihuanxuanhuan',
                ],
                [
                    'parent_id' => 0,
                    'title'     => '武侠仙侠',
                    'slug'      => 'wuxiaxianxia',
                ],
                [
                    'parent_id' => 0,
                    'title'     => '历史军事',
                    'slug'      => 'lishijunshi',
                ],
                [
                    'parent_id' => 0,
                    'title'     => '都市娱乐',
                    'slug'      => 'dushiyule',
                ],
                [
                    'parent_id' => 0,
                    'title'     => '科幻游戏',
                    'slug'      => 'kehuanyouxi',
                ],
                [
                    'parent_id' => 0,
                    'title'     => '悬疑灵异',
                    'slug'      => 'xuanyilingyi',
                ],
                [
                    'parent_id' => 0,
                    'title'     => '体育同人',
                    'slug'      => 'tiyutongren',
                ],
                [
                    'parent_id' => 1,
                    'title'     => '东方玄幻',
                    'slug'      => 'dongfangxuanhuan',
                ],
                [
                    'parent_id' => 1,
                    'title'     => '西方奇幻',
                    'slug'      => 'xifangqihuan',
                ],
                [
                    'parent_id' => 1,
                    'title'     => '异世大陆',
                    'slug'      => 'yishidalu',
                ],
                [
                    'parent_id' => 1,
                    'title'     => '王朝争霸',
                    'slug'      => 'wangchaozhengba',
                ],
                [
                    'parent_id' => 1,
                    'title'     => '转世重生',
                    'slug'      => 'zhuanshichongsheng',
                ],
                [
                    'parent_id' => 1,
                    'title'     => '异术超能',
                    'slug'      => 'yishuchaoneng',
                ],
                [
                    'parent_id' => 1,
                    'title'     => '上古神话',
                    'slug'      => 'shanggushenhua',
                ],
                [
                    'parent_id' => 1,
                    'title'     => '魔法校园',
                    'slug'      => 'mofaxiaoyuan',
                ],
                [
                    'parent_id' => 1,
                    'title'     => '吸血传奇',
                    'slug'      => 'xixuechuanqi',
                ],
                [
                    'parent_id' => 2,
                    'title'     => '古典仙侠',
                    'slug'      => 'gudianxianxia',
                ],
                [
                    'parent_id' => 2,
                    'title'     => '现代修真',
                    'slug'      => 'xiandaixiuzhen',
                ],
                [
                    'parent_id' => 2,
                    'title'     => '新派武侠',
                    'slug'      => 'xinpaiwuxia',
                ],
                [
                    'parent_id' => 2,
                    'title'     => '传统武侠',
                    'slug'      => 'chuantongwuxia',
                ],
                [
                    'parent_id' => 2,
                    'title'     => '奇幻修真',
                    'slug'      => 'qihuanxiuzhen',
                ],
                [
                    'parent_id' => 3,
                    'title'     => '历史架空',
                    'slug'      => 'lishijiakong',
                ],
                [
                    'parent_id' => 3,
                    'title'     => '穿越历史',
                    'slug'      => 'chuanyuelishi',
                ],
                [
                    'parent_id' => 3,
                    'title'     => '历史传记',
                    'slug'      => 'lishizhuanji',
                ],
                [
                    'parent_id' => 3,
                    'title'     => '军旅生涯',
                    'slug'      => 'junlvshengya',
                ],
                [
                    'parent_id' => 3,
                    'title'     => '军事战争',
                    'slug'      => 'junshizhanzheng',
                ],
                [
                    'parent_id' => 3,
                    'title'     => '战争幻想',
                    'slug'      => 'zhanzhenghuanxiang',
                ],
                [
                    'parent_id' => 3,
                    'title'     => '抗战烽火',
                    'slug'      => 'kangzhanfenghuo',
                ],
                [
                    'parent_id' => 3,
                    'title'     => '谍战特工',
                    'slug'      => 'diezhantegong',
                ],
                [
                    'parent_id' => 4,
                    'title'     => '都市生活',
                    'slug'      => 'dushishenghuo',
                ],
                [
                    'parent_id' => 4,
                    'title'     => '都市异能',
                    'slug'      => 'dushiyineng',
                ],
                [
                    'parent_id' => 4,
                    'title'     => '都市重生',
                    'slug'      => 'dushichongsheng',
                ],
                [
                    'parent_id' => 4,
                    'title'     => '青春校园',
                    'slug'      => 'qingchunxiaoyuan',
                ],
                [
                    'parent_id' => 4,
                    'title'     => '江湖情仇',
                    'slug'      => 'jianghuqingchou',
                ],
                [
                    'parent_id' => 4,
                    'title'     => '娱乐明星',
                    'slug'      => 'yulemingxing',
                ],
                [
                    'parent_id' => 4,
                    'title'     => '职场商战',
                    'slug'      => 'zhichangshangzhan',
                ],
                [
                    'parent_id' => 5,
                    'title'     => '穿梭时空',
                    'slug'      => 'chuansuoshikong',
                ],
                [
                    'parent_id' => 5,
                    'title'     => '末世危机',
                    'slug'      => 'moshiweiji',
                ],
                [
                    'parent_id' => 5,
                    'title'     => '未来世界',
                    'slug'      => 'weilaishijie',
                ],
                [
                    'parent_id' => 5,
                    'title'     => '古武机甲',
                    'slug'      => 'guwujijia',
                ],
                [
                    'parent_id' => 5,
                    'title'     => '星际文明',
                    'slug'      => 'xingjiwenming',
                ],
                [
                    'parent_id' => 5,
                    'title'     => '超级科技',
                    'slug'      => 'chaojikeji',
                ],
                [
                    'parent_id' => 5,
                    'title'     => '进化异变',
                    'slug'      => 'jinhuayibian',
                ],
                [
                    'parent_id' => 5,
                    'title'     => '电子竞技',
                    'slug'      => 'dianzijingji',
                ],
                [
                    'parent_id' => 5,
                    'title'     => '虚拟网游',
                    'slug'      => 'xuniwangyou',
                ],
                [
                    'parent_id' => 5,
                    'title'     => '游戏异界',
                    'slug'      => 'youxiyijie',
                ],
                [
                    'parent_id' => 5,
                    'title'     => '游戏系统',
                    'slug'      => 'youxixitong',
                ],
                [
                    'parent_id' => 5,
                    'title'     => '游戏主播',
                    'slug'      => 'youxizhubo',
                ],
                [
                    'parent_id' => 6,
                    'title'     => '侦探推理',
                    'slug'      => 'zhentantuili',
                ],
                [
                    'parent_id' => 6,
                    'title'     => '探险生存',
                    'slug'      => 'tanxianshengcun',
                ],
                [
                    'parent_id' => 6,
                    'title'     => '诡秘悬疑',
                    'slug'      => 'guimixuanyi',
                ],
                [
                    'parent_id' => 6,
                    'title'     => '恐怖惊悚',
                    'slug'      => 'kongbujingsong',
                ],
                [
                    'parent_id' => 6,
                    'title'     => '灵异神怪',
                    'slug'      => 'lingyishenguai',
                ],
                [
                    'parent_id' => 6,
                    'title'     => '神秘文化',
                    'slug'      => 'shenmiwenhua',
                ],
                [
                    'parent_id' => 7,
                    'title'     => '篮球运动',
                    'slug'      => 'lanqiuyundong',
                ],
                [
                    'parent_id' => 7,
                    'title'     => '足球运动',
                    'slug'      => 'zuqiuyundong',
                ],
                [
                    'parent_id' => 7,
                    'title'     => '体育赛事',
                    'slug'      => 'tiyusaishi',
                ],
                [
                    'parent_id' => 7,
                    'title'     => '动漫同人',
                    'slug'      => 'dongmantongren',
                ],
                [
                    'parent_id' => 7,
                    'title'     => '影视同人',
                    'slug'      => 'yingshitongren',
                ],
                [
                    'parent_id' => 7,
                    'title'     => '小说同人',
                    'slug'      => 'xiaoshuotongren',
                ],
                [
                    'parent_id' => 7,
                    'title'     => '游戏同人',
                    'slug'      => 'youxitongren',
                ],
            ]
        );
    }
}
