<?php

use App\Models\Follow;
use Illuminate\Database\Seeder;

class FollowsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $follows = factory(Follow::class)->times(500)->make();
        Follow::insertOrIgnore($follows->toArray());
    }
}
