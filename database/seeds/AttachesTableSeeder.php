<?php

use App\Models\Attach;
use Illuminate\Database\Seeder;

class AttachesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $attaches = factory(Attach::class)->times(100)->make();
        Attach::insertOrIgnore($attaches->toArray());
    }
}
