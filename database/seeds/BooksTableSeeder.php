<?php

use App\Models\Book;
use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $books  = factory(Book::class)->times(3000)->make();
        $chunks = array_chunk($books->toArray(), 1000);
        foreach ($chunks as $chunk) {
            Book::insertOrIgnore($chunk);
        }
    }
}
