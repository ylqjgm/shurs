<?php

use App\Models\Chapter;
use Illuminate\Database\Seeder;

class ChaptersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 40000;) {
            $chapters = factory(Chapter::class)->times(1000)->make();
            Chapter::insertOrIgnore($chapters->toArray());
            $chapters = null;
            unset($chapters);
            $chapters = null;

            $i += 1000;
        }
    }
}
