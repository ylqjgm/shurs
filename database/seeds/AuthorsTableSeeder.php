<?php

use App\Models\Author;
use Illuminate\Database\Seeder;

class AuthorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $authors = factory(Author::class)->times(200)->make();
        Author::insertOrIgnore($authors->toArray());
    }
}
