<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\Models\Book::class, function (Faker $faker) {
    $time = $faker->date.' '.$faker->time;

    return [
        'category_id' => $faker->numberBetween(1, 61),
        'author_id'   => $faker->numberBetween(1, 200),
        'poster_id'   => $faker->numberBetween(1, 700),
        'title'       => $faker->name.$faker->jobTitle,
        'slug'        => $faker->slug,
        'intro'       => $faker->text,
        'notice'      => $faker->text,
        'initial'     => strtoupper($faker->randomLetter),
        'words'       => $faker->randomNumber(),
        'views'       => $faker->randomNumber(),
        'follows'     => $faker->randomNumber(),
        'subscribes'  => $faker->randomNumber(),
        'full'        => $faker->boolean,
        'vip'         => $faker->boolean,
        'status'      => $faker->boolean,
        'created_at'  => $time,
        'updated_at'  => $time,
    ];
});
