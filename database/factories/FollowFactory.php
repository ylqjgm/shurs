<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\Follow;
use Faker\Generator as Faker;

$factory->define(Follow::class, function (Faker $faker) {
    return [
        'book_id'    => $faker->numberBetween(1, 3000),
        'user_id'    => $faker->numberBetween(1, 700),
        'chapter_id' => $faker->numberBetween(1, 40000),
    ];
});
