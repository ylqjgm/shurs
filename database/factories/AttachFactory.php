<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\Models\Attach::class, function (Faker $faker) {
    $time = $faker->date.' '.$faker->time;

    return [
        'chapter_id' => $faker->numberBetween(1, 40000),
        'name'       => $faker->unixTime,
        'extension'  => $faker->fileExtension,
        'size'       => $faker->randomNumber(),
        'created_at' => $time,
        'updated_at' => $time,
    ];
});
