<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\Models\Chapter::class, function (Faker $faker) {
    $time = $faker->date.' '.$faker->time;
    $is_volume = $faker->boolean;

    return [
        'book_id'    => $faker->numberBetween(1, 3000),
        'volume_id'  => $is_volume ? 0 : $faker->numberBetween(1, 40000),
        'title'      => $faker->name.$faker->jobTitle,
        'words'      => $faker->randomNumber(),
        'orders'     => $faker->randomNumber(),
        'is_volume'  => $is_volume,
        'status'     => $faker->boolean,
        'created_at' => $time,
        'updated_at' => $time,
    ];
});
