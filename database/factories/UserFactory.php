<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\Models\User::class, function (Faker $faker) {
    $time = $faker->date.' '.$faker->time;

    return [
        'username'          => $faker->userName,
        'nicname'           => $faker->name,
        'email'             => $faker->safeEmail,
        'password'          => \Illuminate\Support\Facades\Hash::make('123456'),
        'email_verified_at' => now(),
        'status'            => $faker->boolean,
        'remember_token'    => \Illuminate\Support\Str::random(10),
        'created_at'        => $time,
        'updated_at'        => $time,
    ];
});
