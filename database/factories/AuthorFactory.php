<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\Models\Author::class, function (Faker $faker) {
    return [
        'user_id' => $faker->numberBetween(1, 700),
        'penname' => $faker->unique()->name,
        'books'   => $faker->randomNumber(),
    ];
});
