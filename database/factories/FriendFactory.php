<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\Models\Friend::class, function (Faker $faker) {
    $time = $faker->date.' '.$faker->time;

    return [
        'name'        => $faker->company,
        'uri'         => $faker->url,
        'description' => $faker->text(100),
        'orders'      => $faker->numberBetween(0, 59),
        'created_at'  => $time,
        'updated_at'  => $time,
    ];
});
