<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFollowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('follows', function (Blueprint $table) {
            $table->engine    = 'InnoDB';
            $table->charset   = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';

            $table->bigIncrements('id')->unsigned()->nullable(false)->comment('收藏编号');
            $table->unsignedBigInteger('book_id')->nullable(false)->default(0)->comment('小说编号');
            $table->unsignedInteger('user_id')->nullable(false)->default(0)->comment('用户编号');
            $table->unsignedBigInteger('chapter_id')->nullable(false)->default(0)->index()->comment('章节编号');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('follows');
    }
}
