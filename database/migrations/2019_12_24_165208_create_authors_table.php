<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuthorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('authors', function (Blueprint $table) {
            $table->engine    = 'InnoDB';
            $table->charset   = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';

            $table->integerIncrements('id')->unsigned()->nullable(false)->comment('作者编号');
            $table->unsignedInteger('user_id')->nullable(false)->default(0)->unique()->comment('用户编号');
            $table->string('penname', 80)->nullable(false)->default('')->unique()->comment('作者笔名');
            $table->unsignedSmallInteger('books')->nullable(false)->default(0)->comment('作者小说统计');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('authors');
    }
}
