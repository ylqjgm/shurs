<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->engine    = 'InnoDB';
            $table->charset   = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';

            $table->smallIncrements('id')->unsigned()->nullable(false)->comment('分类编号');
            $table->unsignedSmallInteger('parent_id')->nullable(false)->default(0)->comment('上级分类编号');
            $table->string('title', 100)->nullable(false)->default('')->comment('分类名称');
            $table->string('slug', 200)->nullable(false)->default('')->index()->comment('分类标记');
            $table->unsignedInteger('books')->nullable(false)->default(0)->comment('小说数量');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
