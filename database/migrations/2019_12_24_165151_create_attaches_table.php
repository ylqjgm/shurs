<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttachesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attaches', function (Blueprint $table) {
            $table->engine    = 'InnoDB';
            $table->charset   = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';

            $table->integerIncrements('id')->unsigned()->nullable(false)->comment('附件编号');
            $table->unsignedBigInteger('chapter_id')->nullable(false)->default(0)->index()->comment('所属章节编号');
            $table->unsignedInteger('user_id')->nullable(false)->default(0)->index()->comment('所属用户编号');
            $table->string('name', 100)->nullable(false)->default('')->comment('附件名称');
            $table->string('extension', 10)->nullable(false)->default('')->comment('附件后缀');
            $table->unsignedInteger('size')->nullable(false)->default(0)->comment('附件大小');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attaches');
    }
}
