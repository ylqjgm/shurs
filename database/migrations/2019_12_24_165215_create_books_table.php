<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->engine    = 'InnoDB';
            $table->charset   = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';

            $table->bigIncrements('id')->unsigned()->nullable(false)->comment('小说编号');
            $table->unsignedSmallInteger('category_id')->nullable(false)->default(0)->index()->comment('分类编号');
            $table->unsignedInteger('author_id')->nullable(false)->default(0)->index()->comment('作者编号');
            $table->unsignedInteger('poster_id')->nullable(false)->default(0)->comment('发布者编号');
            $table->string('title', 100)->nullable(false)->default('')->index()->comment('小说名称');
            $table->string('slug', 200)->nullable(false)->default('')->index()->comment('小说标记');
            $table->text('intro')->comment('小说简介');
            $table->text('notice')->comment('小说公告');
            $table->unsignedSmallInteger('chapters')->nullable(false)->default(0)->comment('章节数量');
            $table->char('initial', 1)->nullable(false)->default('')->index()->comment('归属首字母');
            $table->unsignedInteger('words')->nullable(false)->default(0)->index()->comment('小说字数');
            $table->unsignedInteger('views')->nullable(false)->default(0)->comment('阅读次数');
            $table->unsignedInteger('follows')->nullable(false)->default(0)->comment('收藏数量');
            $table->unsignedInteger('subscribes')->nullable(false)->default(0)->comment('订阅数量');
            $table->boolean('full')->nullable(false)->default(true)->comment('是否全本');
            $table->boolean('vip')->nullable(false)->default(false)->comment('是否VIP');
            $table->boolean('status')->nullable(false)->default(true)->index()->comment('小说状态');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
