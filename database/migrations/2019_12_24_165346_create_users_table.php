<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->engine    = 'InnoDB';
            $table->charset   = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';

            $table->integerIncrements('id')->unsigned()->nullable(false)->comment('用户编号');
            $table->string('username', 30)->nullable(false)->default('')->unique()->comment('用户名称');
            $table->string('nicname', 60)->nullable(false)->default('')->unique()->comment('用户昵称');
            $table->string('email', 60)->nullable(false)->default('')->unique()->comment('用户邮箱');
            $table->char('password', 96)->nullable(false)->default('')->comment('用户密码');
            $table->timestamp('email_verified_at')->nullable(false)->comment('验证时间');
            $table->boolean('status')->nullable(false)->default(true)->index()->comment('用户状态');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
