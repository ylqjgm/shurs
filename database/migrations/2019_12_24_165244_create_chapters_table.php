<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChaptersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chapters', function (Blueprint $table) {
            $table->engine    = 'InnoDB';
            $table->charset   = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';

            $table->bigIncrements('id')->unsigned()->nullable(false)->comment('章节编号');
            $table->unsignedBigInteger('book_id')->nullable(false)->default(0)->index()->comment('小说编号');
            $table->unsignedBigInteger('volume_id')->nullable(false)->default(0)->index()->comment('分卷编号');
            $table->string('title', 100)->nullable(false)->default('')->comment('章节名称');
            $table->unsignedInteger('words')->nullable(false)->default(0)->comment('章节字数');
            $table->unsignedSmallInteger('orders')->nullable(false)->default(0)->comment('章节排序');
            $table->boolean('is_volume')->nullable(false)->default(false)->comment('是否为分卷');
            $table->boolean('status')->nullable(false)->default(true)->comment('章节状态');
            $table->timestamps();

            $table->index(['book_id', 'orders'], 'book_id', 'btree');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chapters');
    }
}
