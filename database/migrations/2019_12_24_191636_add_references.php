<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddReferences extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('attaches', function (Blueprint $table) {
            $table->foreign('chapter_id')
                  ->references('id')
                  ->on('chapters')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
        });

        Schema::table('follows', function (Blueprint $table) {
            $table->foreign('book_id')
                  ->references('id')
                  ->on('books')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');

            $table->foreign('user_id')
                  ->references('id')
                  ->on('users')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
        });

        Schema::table('chapters', function (Blueprint $table) {
            $table->foreign('book_id')
                  ->references('id')
                  ->on('books')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('attaches', function (Blueprint $table) {
            $table->dropForeign(['chapter_id']);
        });

        Schema::table('follows', function (Blueprint $table) {
            $table->dropForeign(['book_id']);
            $table->dropForeign(['user_id']);
        });

        Schema::table('chapters', function (Blueprint $table) {
            $table->dropForeign(['book_id']);
        });
    }
}
