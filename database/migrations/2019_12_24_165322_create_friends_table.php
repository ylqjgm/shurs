<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFriendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('friends', function (Blueprint $table) {
            $table->engine    = 'InnoDB';
            $table->charset   = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';

            $table->integerIncrements('id')->unsigned()->nullable(false)->comment('友链编号');
            $table->string('name', 100)->nullable(false)->default('')->comment('友链名称');
            $table->string('uri', 200)->nullable(false)->default('')->comment('友链地址');
            $table->text('description')->comment('友链描述');
            $table->unsignedSmallInteger('orders')->nullable(false)->default(0)->index()->comment('友链排序');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('friends');
    }
}
