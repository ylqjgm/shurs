<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configs', function (Blueprint $table) {
            $table->engine    = 'InnoDB';
            $table->charset   = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';

            $table->string('name', 50)->nullable(false)->primary()->comment('配置名称');
            $table->string('title', 100)->nullable(false)->default('')->comment('显示名称');
            $table->text('description')->comment('配置描述');
            $table->string('module', 50)->nullable(false)->default('')->comment('所属模块');
            $table->text('value')->comment('配置数据');
            $table->text('define')->comment('默认配置');
            $table->text('view')->comment('显示选项');
            $table->unsignedInteger('orders')->nullable(false)->default(0)->index()->comment('排序');

            $table->unique(['module', 'name']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configs');
    }
}
