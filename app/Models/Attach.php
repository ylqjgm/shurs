<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Attach.
 *
 * @property-read \App\Models\Chapter $chapter
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Attach newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Attach newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Attach query()
 * @mixin \Eloquent
 * @property-read \App\Models\User $user
 * @property int $id 附件编号
 * @property int $chapter_id 所属章节编号
 * @property int $user_id 所属用户编号
 * @property string $name 附件名称
 * @property string $extension 附件后缀
 * @property int $size 附件大小
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Attach whereChapterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Attach whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Attach whereExtension($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Attach whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Attach whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Attach whereSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Attach whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Attach whereUserId($value)
 */
class Attach extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function chapter()
    {
        return $this->belongsTo(Chapter::class);
    }
}
