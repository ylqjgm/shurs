<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Friend.
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Friend newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Friend newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Friend query()
 * @mixin \Eloquent
 * @property int $id 友链编号
 * @property string $name 友链名称
 * @property string $uri 友链地址
 * @property string $description 友链描述
 * @property int $orders 友链排序
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Friend whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Friend whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Friend whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Friend whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Friend whereOrders($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Friend whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Friend whereUri($value)
 */
class Friend extends Model
{
    //
}
