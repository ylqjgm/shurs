<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Book.
 *
 * @property-read \App\Models\Author                                             $author
 * @property-read \App\Models\Category                                           $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Chapter[] $chapters
 * @property-read int|null                                                       $chapters_count
 * @property-read \App\Models\User                                               $poster
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book query()
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Follow[] $follows
 * @property-read int|null $follows_count
 * @property int $id 小说编号
 * @property int $category_id 分类编号
 * @property int $author_id 作者编号
 * @property int $poster_id 发布者编号
 * @property string $title 小说名称
 * @property string $slug 小说标记
 * @property string $intro 小说简介
 * @property string $notice 小说公告
 * @property string $initial 归属首字母
 * @property int $words 小说字数
 * @property int $views 阅读次数
 * @property int $subscribes 订阅数量
 * @property int $full 是否全本
 * @property int $vip 是否VIP
 * @property int $status 小说状态
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereChapters($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereFollows($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereFull($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereInitial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereIntro($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereNotice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book wherePosterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereSubscribes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereViews($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereVip($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Book whereWords($value)
 */
class Book extends Model
{
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function author()
    {
        return $this->belongsTo(Author::class);
    }

    public function poster()
    {
        return $this->belongsTo(User::class);
    }

    public function chapters()
    {
        return $this->hasMany(Chapter::class);
    }

    public function follows()
    {
        return $this->belongsToMany(Follow::class);
    }
}
