<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\User.
 *
 * @property-read \App\Models\Author                                            $author
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Follow[] $follows
 * @property-read int|null                                                      $follows_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User query()
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Book[]   $books
 * @property-read int|null                                                      $books_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Attach[] $attach
 * @property-read int|null $attach_count
 * @property int $id 用户编号
 * @property string $username 用户名称
 * @property string $nicname 用户昵称
 * @property string $email 用户邮箱
 * @property string $password 用户密码
 * @property string $email_verified_at 验证时间
 * @property int $status 用户状态
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereNicname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUsername($value)
 */
class User extends Model
{
    public function attach()
    {
        return $this->hasMany(Attach::class);
    }

    public function author()
    {
        return $this->hasOne(Author::class);
    }

    public function follows()
    {
        return $this->belongsToMany(Follow::class);
    }

    public function books()
    {
        return $this->hasMany(Book::class);
    }
}
