<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Follow.
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Book[] $book
 * @property-read int|null $book_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $user
 * @property-read int|null $user_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Follow newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Follow newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Follow query()
 * @mixin \Eloquent
 * @property int $id 收藏编号
 * @property int $book_id 小说编号
 * @property int $user_id 用户编号
 * @property int $chapter_id 章节编号
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Follow whereBookId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Follow whereChapterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Follow whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Follow whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Follow whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Follow whereUserId($value)
 */
class Follow extends Model
{
    public function user()
    {
        return $this->belongsToMany(User::class);
    }

    public function book()
    {
        return $this->belongsToMany(Book::class);
    }
}
