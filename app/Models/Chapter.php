<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Chapter.
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Attach[] $attaches
 * @property-read int|null $attaches_count
 * @property-read \App\Models\Book $book
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chapter newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chapter newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chapter query()
 * @mixin \Eloquent
 * @property int $id 章节编号
 * @property int $book_id 小说编号
 * @property int $volume_id 分卷编号
 * @property string $title 章节名称
 * @property int $words 章节字数
 * @property int $orders 章节排序
 * @property int $is_volume 是否为分卷
 * @property int $status 章节状态
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chapter whereBookId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chapter whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chapter whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chapter whereIsVolume($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chapter whereOrders($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chapter whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chapter whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chapter whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chapter whereVolumeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chapter whereWords($value)
 */
class Chapter extends Model
{
    public function attaches()
    {
        return $this->hasMany(Attach::class);
    }

    public function book()
    {
        return $this->belongsTo(Book::class);
    }
}
