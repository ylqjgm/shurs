<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Config.
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Config newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Config newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Config query()
 * @mixin \Eloquent
 * @property string $name 配置名称
 * @property string $title 显示名称
 * @property string $description 配置描述
 * @property string $module 所属模块
 * @property string $value 配置数据
 * @property string $define 默认配置
 * @property string $view 显示选项
 * @property int $orders 排序
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Config whereDefine($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Config whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Config whereModule($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Config whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Config whereOrders($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Config whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Config whereValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Config whereView($value)
 */
class Config extends Model
{
    //
}
